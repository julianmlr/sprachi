import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# Use a service account
cred = credentials.Certificate('quastakita-918feac88080.json')
firebase_admin.initialize_app(cred)

db = firestore.client()


docs = db.collection(u'Children').stream()

#i = 0

#for doc in docs:
 #   i = i + 1
  #  print(i)
   # print(u'{} => {}'.format(doc.id, doc.to_dict()))

# Replace some unnice charaters
#for doc in docs:
    #doc.to_dict().replace("'", '"')
    #str(doc.to_dict).replace("None", '"None"')
    #doc.to_dict().replace("False", "false")
    #doc.to_dict().replace("True", "true")


try:
    my_file_handle = open("quastaQuestionsData.json", "w")
    my_file_handle.write('[')
    for doc in docs:
        my_file_handle.write(str(doc.to_dict()).replace("'", '"').replace("None", '"None"').replace("False", "false").replace("True", "true"))
        #print(doc.to_dict(), "\n")
    my_file_handle.write("]")
    my_file_handle.close()
except IOError:
    print("Das war nix")
finally:
    print("exit")



