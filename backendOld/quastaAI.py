
from __future__ import print_function

import operator  # for sorting dictionaries
import random
import h2o
import numpy as np  # Array, Vector and Matrix calulations
import pandas as pd  # DataFrame handling
from h2o.estimators.gbm import H2OGradientBoostingEstimator  # for GBM
from h2o.estimators.glm import H2OGeneralizedLinearEstimator  # for LIME

h2o.init(max_mem_size='2G')
h2o.remove_all()

x = []

amountOfChildren = 1000
# Part A - Yes / No questions
for j1 in range(12):
    x1 = []
    for i in range(amountOfChildren):
        x1.append(random.randint(0, 1))
    x.append(x1)

# Part A,B and C
for j2 in range(27):
    x2 = []
    for i in range(amountOfChildren):
        x2.append(random.randint(0, 3))
    x.append(x2)

# Meta Data: Motherlanguage
for j3 in range(1):
    x3 = []
    for i in range(amountOfChildren):
        x3.append(random.randint(0, 1))
    x.append(x3)

# Meta Data: Age in month
for j4 in range(1):
    x4 = []
    for i in range(amountOfChildren):
        x4.append(random.randint(48, 72))
    x.append(x4)

sums = []
for i in range(0, amountOfChildren):
    sum = 0
    supportNeeded = 0
    for j in range(len(x) - 1):
        sum = sum + x[j][i]
        if sum > 55:
            supportNeeded = 0
        elif sum < 55:
            supportNeeded = 1
    sums.append(supportNeeded)
x.append(sums)

partAHuepfen = pd.Series(x[0])
partAEinenBallFangen = pd.Series(x[1])
dataFrame = pd.DataFrame({'partA.huepfen': pd.Series(x[0]), 'partA.einenBallFangen': pd.Series(x[1]),
                          'partA.koerperteileZeigen': pd.Series(x[2]), 'partA.knoepfen': pd.Series(x[3]),
                          'partA.einfacheMusterImSandNachmalen': pd.Series(x[4]),
                          'partA.einBlattEckeaufEckefalten': pd.Series(x[5]),
                          'partA.durchEinenTrinkhalmPusten': pd.Series(x[6]),
                          'partA.wattebauschInEineRichtungPusten': pd.Series(x[7]),
                          'partA.grimassenSchneiden': pd.Series(x[8]),
                          'partA.geraeuscheUnterscheiden': pd.Series(x[9]),
                          'partA.dieRichtungEinesTonsHoeren': pd.Series(x[10]),
                          'partA.einenRhythmusNachklatschen': pd.Series(x[11]),
                          'partB.woerterNachsprechen': pd.Series(x[13]),
                          'partB.fantasiewoerterNachsprechen': pd.Series(x[14]),
                          'partB.zweiUndMehrsilbilgeWoerterKlatschen': pd.Series(x[15]),
                          'partB.reimwoerterErkennen': pd.Series(x[16]),
                          'partB.fehlendeReimwoerterErgaenzen': pd.Series(x[17]),
                          'partC.deutlicheAussprache': pd.Series(x[17]),
                          'partC.fluessigesSprechen': pd.Series(x[18]),
                          'partC.sichFuerNeueWoerterInteressieren': pd.Series(x[19]),
                          'partC.sichNeueWoerterAneignen': pd.Series(x[20]),
                          'partC.oberbegriffeVerstehen': pd.Series(x[21]),
                          'partC.mimikGestikEinsetzenWennPassendeWoerterFehlen': pd.Series(x[22]),
                          'partC.nachfragenWennEtwasNichtVerstandenWird': pd.Series(x[23]),
                          'partC.interesseZeigenGernTeilnehmenAnGespraechen': pd.Series(x[24]),
                          'partC.eigeneGedankenAeussern': pd.Series(x[25]),
                          'partC.zuhoerenKoennen': pd.Series(x[26]),
                          'partC.vonEigenenErlebnissenErzaehlen': pd.Series(x[27]),
                          'partC.andereVerstehen': pd.Series(x[28]),
                          'partC.auftraegeVerstehen': pd.Series(x[29]),
                          'partC.sichMitEinemAnderenKindAbsprechen': pd.Series(x[30]),
                          'partD.gernGeschichtenVorlesenLassen': pd.Series(x[31]),
                          'partD.selbstBilderbuecherAnsehen': pd.Series(x[32]),
                          'partD.beimVorlesenZuhoeren': pd.Series(x[33]),
                          'partD.sagenWasAnEinerGeschichtegefaellt': pd.Series(x[34]),
                          'partD.aufPersonenUndHandlungenInEinerGeschichteEingehen': pd.Series(x[35]),
                          'partD.erzaehlenWasAufBildernGeschieht': pd.Series(x[36]),
                          'partD.fragenZurGeschichteStellen': pd.Series(x[37]),
                          'partD.denInhaltMitEigenenErlebnissenVerbinden': pd.Series(x[38]),
                          'checkGermanMotherlanguage': pd.Series(x[39]),
                          'currentAgeInMonth': pd.Series(x[40]),
                          'target': pd.Series(x[41]),
                          })

# Assign the shorthand y to the prediction target and all other input variables to X
y = 'target'
X = [name for name in dataFrame.columns if name not in [y]]




def recode_cc_data(data):
    two_options_dict = {0: 'no', 1: 'yes'}
    four_options_dict = {0: 'never', 1: 'rare', 2: 'mostly', 3: 'always'}
    motherlanguage_dict = {0: 'german', 1: 'non german'}

    for name in data.columns:
        if name in ['partA.huepfen', 'partA.einenBallFangen', 'partA.koerperteileZeigen', 'partA.knoepfen',
                    'partA.einfacheMusterImSandNachmalen', 'partA.einBlattEckeaufEckefalten',
                    'partA.durchEinenTrinkhalmPusten', 'partA.wattebauschInEineRichtungPusten',
                    'partA.grimassenSchneiden', 'partA.geraeuscheUnterscheiden', 'partA.dieRichtungEinesTonsHoeren',
                    'partA.einenRhythmusNachklatschen' ]:
            data[name] = data[name].apply(lambda i: two_options_dict[i])

    for name in data.columns:
        if name in ['partB.woerterNachsprechen',
                    'partB.fantasiewoerterNachsprechen', 'partB.zweiUndMehrsilbilgeWoerterKlatschen',
                    'partB.reimwoerterErkennen', 'partB.fehlendeReimwoerterErgaenzen', 'partC.deutlicheAussprache',
                    'partC.fluessigesSprechen', 'partC.sichFuerNeueWoerterInteressieren',
                    'partC.sichNeueWoerterAneignen', 'partC.oberbegriffeVerstehen',
                    'partC.mimikGestikEinsetzenWennPassendeWoerterFehlen',
                    'partC.nachfragenWennEtwasNichtVerstandenWird', 'partC.interesseZeigenGernTeilnehmenAnGespraechen',
                    'partC.eigeneGedankenAeussern', 'partC.zuhoerenKoennen', 'partC.vonEigenenErlebnissenErzaehlen',
                    'partC.andereVerstehen', 'partC.auftraegeVerstehen', 'partC.sichMitEinemAnderenKindAbsprechen',
                    'partD.gernGeschichtenVorlesenLassen', 'partD.selbstBilderbuecherAnsehen',
                    'partD.beimVorlesenZuhoeren',
                    'partD.sagenWasAnEinerGeschichtegefaellt',
                    'partD.aufPersonenUndHandlungenInEinerGeschichteEingehen', 'partD.erzaehlenWasAufBildernGeschieht',
                    'partD.fragenZurGeschichteStellen', 'partD.denInhaltMitEigenenErlebnissenVerbinden']:
            data[name] = data[name].apply(lambda i: four_options_dict[i])
            
    data['checkGermanMotherlanguage'] = data['checkGermanMotherlanguage'].apply(lambda i: motherlanguage_dict[i])

    return h2o.H2OFrame(data)


dataFrame = recode_cc_data(dataFrame)

print(dataFrame)

dataFrame[y] = dataFrame[y].asfactor()


# Split into training and validation
train, test = dataFrame.split_frame([0.7], seed=12345)

print('Train data rows = %d, columns = %d' % (train.shape[0], train.shape[1]))
print('Test data rows = %d, columns = %d' % (train.shape[0], train.shape[1]))

# Initialize GBM model

model = H2OGradientBoostingEstimator(ntrees = 150,
                                     max_depth = 4,
                                     col_sample_rate = 0.9,
                                     stopping_rounds = 5,
                                     score_tree_interval = 1,
                                     seed = 12345)

# Train a GBM model
model.train(y=y, x=X, training_frame=train, validation_frame=test)

# Print AUC
print('GBM Test AUC = %.2f' % model.auc(valid=True))

selectedRow = test.iloc[40]
print(selectedRow)


####################################
# Explainable Part
####################################

def generate_local_sample(row, frame, X, N=1000):
    # Initialize Pandas DataFrame
    sample_frame = pd.DataFrame(data=np.zeros(shape=(N, len(X))), columns=X)

    for key, val in frame[X].types.item():
        if val == 'enum':
            rs = np.random.RandomState(111111)
            draw = rs.choice(frame[key].levels()[0], size=(1, N)) [0]
        else:
            rs = np.random.RandomState(11111)
            loc = row[key][0, 0]
            sd = frame[key].sd()
            draw = rs.normal(loc, sd, (N, 1))
            draw[draw < 0 ] = loc

        sample_frame[key] = draw

    return sample_frame

# Run and display results

perturbed_sample = generate_local_sample(selectedRow, test, X)
perturbed_sample.head(n = 3)



# scaling and one-hot encoding for calculating Euclidian distance
# for the row of interest

# scale numeric
numeric = list(set(X) - set(['ID', 'SEX', 'EDUCATION', 'MARRIAGE', 'PAY_0', 'PAY_2',
                             'PAY_3', 'PAY_4', 'PAY_5', 'PAY_6', 'DEFAULT_NEXT_MONTH']))

scaled_test = test.as_data_frame()
scaled_test[numeric] = (scaled_test[numeric] - scaled_test[numeric].mean())/scaled_test[numeric].std()
    
# encode categorical
row_df = scaled_test[scaled_test['ID'] == 22760]
row_dummies = pd.concat([row_df.drop(['ID', 'SEX', 'EDUCATION', 'MARRIAGE', 'PAY_0', 'PAY_2',
                                      'PAY_3', 'PAY_4', 'PAY_5', 'PAY_6', 'DEFAULT_NEXT_MONTH'], axis=1),
                        pd.get_dummies(row_df[['SEX', 'EDUCATION', 'MARRIAGE', 'PAY_0',
                                               'PAY_2', 'PAY_3', 'PAY_4', 'PAY_5', 'PAY_6']])], 
                        axis=1)

# convert to H2OFrame
row_dummies = h2o.H2OFrame(row_dummies)
row_dummies
